This repo builds an Ubuntu 18.04 live server based container with MuslLibC.

It has been tested on the following Operating Sytem:
```
 - CentOS 7
 - Ubuntu 18.04
```

To run, clone the repo to a directory of your choice and navigate to the directory.

Enter 

` sudo ./run_musl.sh `

then enter your password and let the process do the rest.

The following packages are installed in the container:

```
 - oppenssh-server 
 - python3-pip
 - nim
 - ruby-all
 - musl
 - vim
 - tmux 
```

Best way to log in remotely after build for testing is by using the following ssh command:

` ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o "LogLevel ERROR" USER@CONTAINER IP `

Logging in this way accomplishes three goals:

```
 - does not prompt for the ECDSA key
 - does not add the Container IP as a known host in .ssh/known_hosts directory
 - turns off any warnings 
```

However, If you do have issues with the testing ssh remote access within the host machine you set up your container,  navigate to your 

` ~/.ssh/known_hosts `

directory and make sure there isnt a key set up for the container's ip address and try again.


 

